# Conference booklet made easy (hopefully)

This repository contains a `python` script and `tex` files to generate conference booklets from abstracts written in separate yaml files.

The process is mostly automated. Typical workflow:

1. each presentation needs a separate abstract file (see under `sample/abstracts` for examples).
2. you can tweak some colours in `conference-programme-theming.tex`
3. you may need to adapt a few little things in `pgm-tmpl.tex` if you want to tweak the content
4. set up your configuration in `config.yaml` where you basically define:
   1. the metadata of your booklet (e.g., title, date, venue)
   2. the sessions with the talks
   3. you run the `python` command
   4. enjoy (hopefully)

## Foreword

See the doc in `programme-builder.py` and an example in the `sample` folder for more details.

The `config.yaml` file creates the binding between the abstracts (to be provided into separated `yaml` files), and the final programme to be generated.

## Depends on

- `python3`
- see `requirements.txt` file

## Usage

The script uses `argparse`, so you can type `python programme-builder.py -h` for a full description. An example with current sample (generating example output in `out` folder):

```bash
python programme-builder.py \
        -f sample/config.yaml \
        -a sample/abstracts/seng402_abstract_ \
        -e .txt \
        -t pgm-tmpl.tex \
        -o sample/out/output.tex \
        -p ../../theme/ \
        -g -d
```

## Credits

This utility has been adapted from the work of Maxime Lucas, Pau Clusella and Thomas Kreuz. The original files are available at (https://github.com/maximelucas/AMCOS_booklet).

## License

This utility is licensed under GPL 3.
