#!/usr/bin/env python
#
# Generate conference programme for SENG402.
# See description before main part of programme for example of command (end of file).
#
# Requires all abstracts of presentations in yaml format:
#
# ---
# title: "Your project title"
# name: each name separated by a comma, if more than one, e.g., "Martin Jones, Mark Much, Michelle Sel"
# sponsor: Sponsor name/s, each name separated by a comma, if more than one, e.g., "University of Canterbury, Department of Conservation"
# supervisor: Full name of your supervisor, e.g., "Prof. Rick O'Chet"
# type: one value of "personal", "industry", "research"
# abstract: |
#   Free text. Will be interpreted by a latex engine, Beware of reserved keywords (e.g., &) that need to be "escaped"
#   with one backslash in the abstract field and two in the other fields, e.g., \&.
# ---
#
# Make sure the config.yaml file is setup properly (hopefully it is self-explanatory, see also read_config function):
#
#   title / subtitle: Your booklet sub/title
#   venue: the venue of the conference
#   date: the date of the conference
#   forewords: there will be a page with forewords in the booklet, input your latex-aware text.
#   signature: if you want a special signature at the end of the forewords (latex typeset)
#   sponsor: the booklet has a page at the end for sponsors, this is the (relative to output file) link to the image
#   logo: any logo you want to add in the front page (top part, above title, path is relative to output file)
#   footer: if you want a footer image on all pages (path is relative to output file)
#   programme: this is where you define the programme of the conference. You can create as many tracks as you like
#       (but more than two may cause issues for the tables at the beginning of the booklet, untested).
#       Each track is defined by:
#
#     track: name of the track
#     room: the room where the presentation will happen
#     sessions: a session is composed of:
#       at least a time and a name
#       an optional session chair
#       an optional list of presentations:
#         time of the presentation
#         project is the suffix of the abstract file name, e.g., if all abstracts are named abstract_x, specify x here
#
#
# Requires a template latex file to generate final tex. A sample latex file with a working template should be provided next to this script.
#
# That's pretty much it. Look into the code, it is documented, so hopefully, it'll answer your questions.
#
# Example of usage with files in YEAR folder:
#     $ python programme-builder.py \
#         -f YEAR/config.yaml \
#         -a YEAR/abstracts/seng402_abstract_ \
#         -e .txt \
#         -t YEAR/seng402_showcase-pgm-template.tex \
#         -o YEAR/seng402_showcase-pgm-YEAR.tex \
#         -p relative/path/to/sty/from/output
#         -g -d
# author: Fabian Gilson, fabgilson.github.io
#

import argparse
import os
import pathlib
import subprocess
import yaml


def read_tex_template(path):
    with open(path) as file:
        return file.read()


def read_config(path):
    """
    Load conference config file (yaml) from given path and return a dictionary with its content.

    All keywords before the "programme" part will be used to replace text in template with same name,
    e.g., foreword will replace $foreword$ in tex template file.

    Keyword arguments:
        path - the path (relative or absolute) to the yaml file to read with all conference config

    Config file structure:
        title: String (title of booklet to produce)
        subtitle: String (subtitle of booklet to produce)
        venue: String
        date: String
        forewords: multiline String (to be put in forewords section, markdown syntax accepted)
        signature: multiline String (to be put as signature of forewords section, markdown syntax accepted)
        sponsor: path to image of sponsors
        sponsortext: multiline String with text to be added below the sponsor image
        finalwords: multiline String with text to be added on the last page of booklet
        programme:
          - track: String (a name for this track)
            room: String (the room where this track will happen)
            sessions:
              - time: String (the time of session)
                name: String (the name of the session)
                chair: String (the person charing the session)
                presentations:
                  - time: String (the presentation time, must be within session time)
                    project: Integer (corresponds to the suffix of an abstract yaml file, content will be pulled from that file)
    """
    with open(path) as file:
        return yaml.safe_load(file)


def read_abstract(path):
    """
    Load abstract file (yaml) from given path and return a dictionary with its content

    Keyword arguments:
        path - the string path (relative or absolute) to the yaml file to read with one abstract

    Config file structure:
        abstract: multiline String (with the abstract of the presentation)
        name: String (presenter names)
        sponsor: String
        supervisor: String (name of supervisor)
        title: String (presentation title)
        type: one of industry / personal / research
    """
    with open(path) as file:
        return yaml.safe_load(file)


def extract_tracks(programme, abstract_path, extension):
    """
    Extract all tracks from given programme and retrieve content of project abstract files

    Keywords arguments:
        programme     - the dictionary containing the full config yaml file
        abstract_path - path to abstracts of the form "some/path/abstractfileprefix"
        extension     - file extension of abstract paths (they may be yaml disguised in txt).

    Returns:
        An array of dictionaries corresponding to the track element in config yaml file (resolving the projects' abstracts files)
    """
    print("\t extracting tracks data")
    tracks = []
    for track in programme:
        track_to_add = {}
        track_to_add["name"] = track["track"]
        track_to_add["room"] = track["room"]
        sessions = []
        for session in track["sessions"]:
            session_to_add = {}
            session_to_add["time"] = session["time"]
            session_to_add["name"] = session["name"]

            if "chair" in session:
                session_to_add["chair"] = session["chair"]
                session_to_add["presentations"] = []

                for presentation in session["presentations"]:
                    presentation_to_add = {}
                    presentation_to_add["time"] = presentation["time"]
                    presentation_to_add.update(
                        read_abstract(abstract_path + str(presentation["project"]) + extension)
                    )

                    session_to_add["presentations"].append(presentation_to_add)

            sessions.append(session_to_add)

        track_to_add["sessions"] = sessions
        tracks.append(track_to_add)

    return tracks


def summary_schedule(tracks):
    """
    Create the summary table with all tracks in each room
    Assume only two tracks

    Keyword arguments:
        tracks - array of dict (time, room, name, chair and array of presentations)

    Returns:
        latex code as follows:

        \begin{center}
          \begin{longtable}{C{0.15\linewidth} C{0.01\linewidth} C{0.3\linewidth}  C{0.3\linewidth}}\hline
            \header{room 1 name}{room 2 name}
            \tablebreak{time}{name}
            \sessionEntry{time}{name session 1a}{chair session 1a}{name session 1b}{chair session 1b}
          \end{longtable}
        \end{center}
    """
    print("\t generating summary schedule")
    summary = "\\begin{center}\n\t\\begin{longtable}{C{0.15\linewidth} C{0.01\linewidth} C{0.3\linewidth} C{0.3\linewidth}}\hline\n"
    summary += "\t\t\\header{" + tracks[0]["room"] + "}{" + tracks[1]["room"] + "}\n"

    for session in tracks[0]["sessions"]:
        if "chair" in session:
            summary += (
                "\t\t\\sessionEntry{"
                + session["time"]
                + "}{"
                + session["name"]
                + "}{"
                + session["chair"]
                + "}{"
            )
            # search for session at the same time in other track
            for other_session in tracks[1]["sessions"]:
                if other_session["time"] == session["time"]:
                    summary += other_session["name"] + "}{" + other_session["chair"] + "}\n"
        else:
            summary += "\t\t\\tablebreak{" + session["time"] + "}{" + session["name"] + "}\n"

    summary += "\t\\end{longtable}\n\\end{center}\n\n"
    return summary


def track_schedule(track):
    """
    Create the detail schedule per track

    Keyword arguments:
        track - dict (time, room, name, chair and array of presentations)

    Returns:
        latex code as follows:

        \trackschedule{track name}{room name}
        \begin{center}
          \footnotesize
          \begin{longtable}{C{0.15\linewidth} C{0.04\linewidth} C{0.31\linewidth} C{0.36\linewidth}}\hline
            \header{Presenter/s}{Project}
            \industryEntry{time}{name}{sponsor}{project title}
            \researchEntry{time}{name}{sponsor}{project title}
            \personalEntry{time}{name}{sponsor}{project title}
            \tablebreak{time}{name}
          \end{longtable}
        \end{center}
    """
    print("\t generating track schedule for " + track["name"])
    schedule = "\\trackschedule{" + track["name"] + "}{" + track["room"] + "}\n\n"
    schedule += "\\begin{center}\n\t\\footnotesize\n"
    schedule += "\t\\begin{longtable}{C{0.15\linewidth} C{0.04\linewidth} C{0.31\linewidth} C{0.36\linewidth}}\hline\n"

    for session in track["sessions"]:
        if "chair" in session:
            for presentation in session["presentations"]:
                if presentation["type"] == "industry":
                    schedule += industry_entry(presentation)

                elif presentation["type"] == "personal":
                    schedule += personal_entry(presentation)

                else:
                    schedule += research_entry(presentation)

        else:
            schedule += "\t\t\\tablebreak{" + session["time"] + "}{" + session["name"] + "}\n"

    schedule += "\t\\end{longtable}\n\\end{center}\n\\newpage\n"
    return schedule


def industry_entry(presentation):
    """
    Generate latex command for industry talks

    Keyword attribute:
        presentation - dict of, amongst other, (time, name, sponsor, title)

    Returns:
        latex code as follows: \industryEntry{time}{author}{sponsor}{title}

    """
    return (
        "\t\t"
        + "\\industryEntry{"
        + presentation["time"]
        + "}{"
        + presentation["name"]
        + "}{"
        + presentation["sponsor"]
        + "}{"
        + presentation["title"]
        + "}\n"
    )


def research_entry(presentation):
    """
    Generate latex command for research talks in schedule

    Keyword attribute:
        presentation - dict of, amongst other, (time, name, sponsor, title)

    Returns:
        latex code as follows: \researchEntry{time}{author}{sponsor}{title}
    """
    return (
        "\t\t"
        + "\\researchEntry{"
        + presentation["time"]
        + "}{"
        + presentation["name"]
        + "}{"
        + presentation["sponsor"]
        + "}{"
        + presentation["title"]
        + "}\n"
    )


def personal_entry(presentation):
    """
    Generate latex command for personal talks (no sponsor) in schedule

    Keyword attribute:
        presentation - dict of, amongst other, (time, name, title)

    Returns:
        latex code as follows: \personalEntry{time}{author}{title}
    """
    return (
        "\t\t"
        + "\\personalEntry{"
        + presentation["time"]
        + "}{"
        + presentation["name"]
        + "}{"
        + presentation["title"]
        + "}\n"
    )


def track_programme(track):
    """
    Generate latex code for track programme (all abstracts)

    Keyword argument:
        track - content of a track, i.e. a dict (time, room, name, chair and array of presentations)

    Returns:
        latex code as follows:

        \chapter{track name}
        \session{session name}{chair}{time}
        \begin{abstract}{project title}{author}{sponsor}{supervisor}{badge}{place, time}
          content
        \end{abstract}

        ... more sessions and abstracts ...

    """
    print("\t generating track programme for " + track["name"])
    programme = "\\chapter{{\\large\\textsc{Track:~~}}" + track["name"] + "}\n\n"
    for session in track["sessions"]:
        if "chair" in session:
            programme += (
                "\\session{" + session["name"] + "}{" + session["chair"] + "}{" + session["time"] + "}\n\n"
            )
            for presentation in session["presentations"]:
                badge = ""
                if presentation["type"] == "industry":
                    badge = "\\industryBadge"
                elif presentation["type"] == "personal":
                    badge = "\\personalBadge"

                else:
                    badge = "\\researchBadge"

                programme += (
                    "\\begin{abstract}{"
                    + presentation["title"]
                    + "}{"
                    + presentation["name"]
                    + "}{"
                    + presentation["sponsor"]
                    + "}{"
                    + presentation["supervisor"]
                    + "}{"
                    + badge
                    + "}{"
                    + track["room"]
                    + ", "
                    + presentation["time"]
                    + "}\n\t"
                    + presentation["abstract"].replace("\n", "\\bigskip\n\n\t")
                    + "\\end{abstract}\n\n"
                )

    return programme


def generate_pdf(latexfile):
    """
    Generate the PDF from given latexfile using 'pdflatex'

    Keyword attribute:
        latexfile - a latexfile to generate
    """

    devnull = open(os.devnull, "w")
    latexlog = open(latexfile + ".log", "w")

    # must run three times to be sure the references are ok (ignore errors from latex because of weird status code)
    for idx in [1, 2, 3]:
        try:
            print("\t generate PDF for the " + str(idx) + " times")
            subprocess.check_call(
                ["pdflatex", "-interaction=nonstopmode", os.path.basename(latexfile)],
                cwd=os.path.dirname(latexfile),
                stdout=latexlog,
                stderr=devnull,
            )

        except subprocess.CalledProcessError as err:
            msg = "\t--> ProcessError - unable to generate PDF. Command was " + " ".join(err.cmd)
            print(msg)
            pass
        except OSError:
            print("OSError - unable to find pdflatex\n")
            raise


"""
Main program, should be self-explanatory

Make sure you have set up the config.yaml file properly first.
"""
if __name__ == "__main__":

    print("Generate programme")
    parser = argparse.ArgumentParser(
        description="Create a conference programme using pdflatex and yaml files"
    )

    parser.add_argument(
        "-f",
        "--file",
        help="Path to conference config file in yaml",
        required=False,
        default="config.yaml",
    )
    parser.add_argument(
        "-a",
        "--abstract",
        help="Path and prefix of (yaml) files containing project abstracts, \
        excluding project number, e.g., abstract/seng402_abstract_",
        required=False,
        default="abstracts/seng402_abstract_",
    )
    parser.add_argument(
        "-e",
        "--extension",
        help="File extension for abstracts ('.' must be included)",
        required=False,
        default=".yaml",
    )
    parser.add_argument("-t", "--texfile", help="conference programme template tex file", required=True)
    parser.add_argument("-o", "--output", help="output tex file", required=False, default="out.tex")
    parser.add_argument("-g", "--genpdf", help="generate PDF file", action="store_true")
    parser.add_argument("-d", "--draft", help="add draft watermarks into PDF file", action="store_true")
    parser.add_argument(
        "-p", "--themepath", help="path to theme and style files (relative to output tex -o)", required=True
    )

    args = vars(parser.parse_args())

    conference = read_config(args["file"])
    tex = read_tex_template(args["texfile"])

    # template path handling
    tex = tex.replace("$themepath$", args["themepath"])

    for key, value in conference.items():
        if key != "programme":
            # we are reading the conference config part
            tex = tex.replace("$" + key + "$", value)
        else:
            # we are reading the programme part, composed of tracks
            tracks = extract_tracks(value, args["abstract"], args["extension"])

            tex = tex.replace("$summary$", summary_schedule(tracks))

            schedule_string = ""
            for track in tracks:
                schedule_string += track_schedule(track)
            tex = tex.replace("$schedule$", schedule_string)

            tracks_string = ""
            for track in tracks:
                tracks_string += track_programme(track)
            tex = tex.replace("$tracks$", tracks_string)

    if args["draft"]:
        tex = tex.replace(
            "$draft$",
            "\\usepackage{draftwatermark}\\SetWatermarkText{draft}\\SetWatermarkScale{1}\\SetWatermarkColor[gray]{0.95}",
        )
    else:
        tex = tex.replace("$draft$", "")

    with open(args["output"], "w") as file:
        file.write(tex)

    if args["genpdf"]:
        generate_pdf(pathlib.Path(file.name).absolute().as_posix())
