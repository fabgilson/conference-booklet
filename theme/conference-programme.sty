%======================================================
% This file is inspired and adapted from 
% "AMCOS_booklet"
% Version 1.1 (04/07/2019)
% A LaTeX template for conference books of abstracts
%
% This template is available at:
% https://github.com/maximelucas/AMCOS_booklet
%
% License: GNU General Public License v3.0
%
% Authors:
% Maxime Lucas (ml.maximelucas@gmail.com)
% Pau Clusella
%
% Current version by Fabian Gilson
%======================================================= 

%
% Define page layout
\usepackage[left=2cm,right=2cm,top=2.5cm,bottom=2cm,outer=1.5cm,inner=2cm]{geometry}

%
% General imports
\usepackage{scrhack}
\usepackage[utf8]{inputenc}
\usepackage[UKenglish]{babel}
\usepackage{graphicx}
\usepackage{multicol}
\usepackage{array}  
\usepackage{longtable}
\usepackage{colortbl}
\usepackage{ifthen}
\usepackage{xstring}

%
% FONT AND TEXT SPECS
%

\usepackage{libertine}
\usepackage[T1]{fontenc}
\renewcommand*\familydefault{\sfdefault} %% Only if the base font of the document is to be sans serif

\usepackage{soul}
\usepackage[scaled=0.9]{FiraMono}
\usepackage{listingsutf8}

%
% Increase line space
\usepackage{setspace}
\setstretch{1.2}

% No indent in paragraphs
\setlength\parindent{0pt}


% GENERAL PAGE STYLING, I.E. PAGE BACKGROUND AND FOOTERS
%

% add outer styling line on pages
\usepackage[scale=1,angle=0,opacity=1]{background}
\backgroundsetup{contents={}}

\AddEverypageHook{%
  \ifthenelse{\thepage > 1}
  {% then
    \ifthenelse{\isodd{\thepage}}
    {
      \backgroundsetup{
        color=\backgroundLightColour,
        position=current page.south east,%
        nodeanchor=south east,
        contents={\rule{10pt}{0.66\paperheight}}
      }              
    }{ % else
      \backgroundsetup{
        color=\backgroundLightColour,
        position=current page.south west,%
        nodeanchor=south west,
        contents={\rule{10pt}{0.66\paperheight}}
      }
    }   
  }{% else, first page, do nothing
  }
  \BgMaterial%
}

% add footer and headers
\usepackage{fancyhdr}
\usepackage{lastpage}
\pagestyle{fancy}

%E: Even page, O: Odd page
%L: Left field, C: Center field, R: Right field
%H: Header, F: Footer
\fancyhead[LE,RO]{}
\fancyhead[LO,RE]{}
\renewcommand{\headrulewidth}{0mm}

\fancyfoot[LO,RE]{\footnotesize \textcolor{\backgroundDarkColour}{\footerImage}}
\fancyfoot[C]{}
\fancyfoot[LE,RO]{\footnotesize \textcolor{\backgroundDarkColour}{\thepage~/~\pageref{LastPage}}}


%
% SECTIONING AND CHAPTER STYLING
%
\setkomafont{chapter}{\bfseries\huge}

%padding between gcolored bar and chapter name
\newdimen\mybarpadding
\mybarpadding=1.5em\relax 

% chapter line format, i.e. thick line next to chapter name
\renewcommand{\chapterlinesformat}[3]{%
  \ifthispageodd{%
    \hfill%
    \raisebox{-0.2em}{%
      \makebox[0pt][r]{\textcolor{\backgroundDarkColour}{\rule{\paperwidth}{1em}}}%
    }%
    \hspace{\mybarpadding}%
    \mbox{#3}%
  }{%
    \mbox{#3}%
    \hspace{\mybarpadding}%
    \raisebox{-0.2em}{%
      \makebox[0pt][l]{\textcolor{\backgroundDarkColour}{\rule{\paperwidth}{1em}}}%
    }%
  }%
}

% spacing around chapter pages
\RedeclareSectionCommand[%
,afterskip=4em plus 1pt minus 1pt%
,beforeskip=-1pt
,level=0%
,toclevel=0%
]{chapter}%

% hide chapter number
\renewcommand\addchaptertocentry[2]{\addtocentrydefault{chapter}{}{#2}}

% apply footers to chapter page (opening right)
\patchcmd{\chapter}{\thispagestyle{plain}}{\thispagestyle{fancy}}{}{}
\fancypagestyle{plain}{%
  \fancyhf{}%
  \fancyfoot[R]{\footnotesize \textcolor{\backgroundDarkColour}{\thepage~/~\pageref{LastPage}}}%
  \fancyfoot[L]{\footnotesize \textcolor{\backgroundDarkColour}{\footerImage}}
  \renewcommand{\headrulewidth}{0mm}%
}

%
% CUSTOM ENVIRONMENT FOR ``SESSIONS'', GROUP OF PRESENTATIONS 
%

\newcommand{\schedule}[1] { %{title}{room}
  {\bfseries\scshape\Large \huge #1}%
  \smallskip\smallskip%
}

\newcommand{\trackschedule}[2] { %{title}{room}
  {\bfseries\scshape\Large Track: \huge #1}%
  {\textcolor{\textcolourLight}{\hspace*{0pt}\hfill #2}}%
  \smallskip\smallskip%
}

\newcommand{\session}[3] { %{title}{chair}{time}
  {\color{\backgroundDarkColour}{\bfseries\scshape Session: {\huge #1}} \hfill (time: #3)}%
  \newline
  {\hfill \textcolor{\textcolourLight}{\itshape Chair: #2}}%
  \smallskip%
  \phantomsection\addcontentsline{toc}{section}{{\scshape #1} \hspace*{.5em} \textcolor{\textcolourLight}{\itshape Chair: #2 (#3)}}%
}


%
% CUSTOM COMMAND FOR ``ABSTRACTS'' OF PRESENTATIONS / PAPERS, e.g.,
%
% Title                  Badge
% Author  Affiliation
% Supervisor: Name      (time slot)
%
% Supervisor can be empty and won't be printed if left empty (4th argument)

\newenvironment{abstract}[6] { %{title}{author}{affiliation/sponsor}{supervisor}{tagtype}{time slot} content
  
  %{\Large \bfseries #1 } {\vspace*{0pt}\hfill #5}
  \setlength{\tabcolsep}{0em}
  \begin{longtable}{L{0.92\linewidth} R{0.08\linewidth}}
    {\Large \bfseries #1 } & {#5}
  \end{longtable}\vspace*{-.5cm} 
  
  \smallskip
  
  {\bfseries \textcolor{\backgroundDarkColour}{\itshape #2}} \hspace*{1em} \textcolor{\textcolourLight}{#3} 
  
  {\footnotesize \textcolor{\textcolourLight}{\ifx&#4& ~ \else Supervisor\IfSubStr{&#4&}{,}{s}{}: #4 \fi {\vspace*{0pt}\hfill (#6)}}}
  
  \smallskip\smallskip
  \phantomsection\addcontentsline{toc}{subsection}{#1 \newline \textcolor{\backgroundDarkColour}{\itshape #2}}
}
{
  \clearpage
}


%
% PAPER BADGES TO BE SHOWN IN TAMETABLE AND ABSTRACT
%
\usepackage{tikz}

\newcommand{\keynoteBadge}{\tikz[baseline={([yshift=-.8ex]current bounding box.center)}]  \node[circle, inner sep=2pt, minimum size=1.6em, color=white, fill=\keynoteColour]{\keynoteLetter};}

\newcommand{\industryBadge}{\tikz[baseline={([yshift=-.8ex]current bounding box.center)}]  \node[circle, inner sep=2pt, minimum size=1.6em, color=white, fill=\industryColour]{\industryLetter};}

\newcommand{\personalBadge}{\tikz[baseline={([yshift=-.8ex]current bounding box.center)}]  \node[circle, inner sep=2pt, minimum size=1.6em, color=white, fill=\personalColour]{\personalLetter};}

\newcommand{\researchBadge}{\tikz[baseline={([yshift=-.8ex]current bounding box.center)}]  \node[circle, inner sep=2pt, minimum size=1.6em, color=white, fill=\researchColour]{\researchLetter};}


%
% TIMETABLE STYLING
%

% column spacing and position
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

%
% Timetable is always composed of 4 columns: time, badge or spacing, ``column 1'' and ``column 2''
% For summary table, column 1 and 2 are for the parallel tracks
% For detailed (track) schedule, column 1 is for the presenter/s, column 2 for the tile
% 

% header rows are composed of two titles (e.g., when having two concurrent tracks/streams)
\newcommand{\header}[2]{\rowcolor{\backgroundDarkColour} % using dark colour as background (same as chapter line)
  & % blank
  & \textcolor{\backgroundLightColour}{#1} % header ``column 1''
  & \textcolor{\backgroundLightColour}{#2} % % header ``column 2''
  \\ \hline % horizontal line
}

% regular entries are composed of a time and content for ``column 1'' and ``column 2''
\newcommand{\regularEntry}[3]{%
  #1 % time
  & % blank
  & #2 % column 1
  & #3 % column 2
  \\ \hline % horizontal line
} 

% session entries are composed of a time, a first session, its first chair, a second session, its other chair
\newcommand{\sessionEntry}[5]{%
  #1 % time
  & % blank
  & #2\par {\footnotesize \textcolor{\textcolourLight} chair: #3} % session 1 and its chair
  & #4\par {\footnotesize \textcolor{\textcolourLight} chair: #5} % session 2 and its chair
  \\ \hline % horizontal line
}

% tablebreak are composed of a time and one row, e.g., tea break
\newcommand{\tablebreak}[2]{\rowcolor{\backgroundLightColour} % using light colour (same as side of pages) as background
  #1 % a time
  & \multicolumn{3}{c}{\bfseries #2} % a column spreading over the remaining of the table
  \\ \hline % horizontal line
}

% keynote and industry entries are composed of time, badge, author, affiliation, title
\newcommand{\keynoteEntry}[4]{%
  #1 % time
  & \keynoteBadge % badge
  & {\bfseries#2}\par {\textcolor{\textcolourLight}{#3}} % author / speaker with affiliation
  & #4 % title
  \\ \hline % horizontal line
}
\newcommand{\industryEntry}[4]{%
  #1 % time
  & \industryBadge % badge
  & {\bfseries#2}\par {\itshape \textcolor{\textcolourLight}{#3}} % author / speaker with affiliation
  & #4 % title
  \\ \hline % horizontal line
}

% research and personal entries are composed of time, badge, author, title
\newcommand{\researchEntry}[4]{%
  #1 % time
  & \researchBadge % badge
  & {\bfseries#2} \par {\itshape \textcolor{\textcolourLight}{#3}} % author / speaker with affiliation
  & #4 % title 
  \\ \hline%
}

\newcommand{\personalEntry}[3]{%
  #1 % time
  & \personalBadge % badge
  & {\bfseries#2} % author / speaker 
  & #3 % title 
  \\ \hline%
}

%
% LOAD HYPERREF LAST
%
\usepackage[hidelinks]{hyperref}
\def\UrlFont{\bfseries\ttfamily}
