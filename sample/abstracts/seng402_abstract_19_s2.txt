title: "CodeWOF Skill Categorisation"
name: "Rebekah McKinnon"
supervisor: "Tim Bell"
sponsor: "University of Canterbury, Computer Science Education Research Group"
type: "research"
abstract: | 
  CodeWOF is a website designed to help New Zealand teachers maintain their programming skills. Born out of EDEM655 and now a recurring SENG402 project, CodeWOF uses simple programming exercises to encourage regular coding. This research considers how categorizing CodeWOF questions into skill sets may help to track users' improving skill level. By considering programming education tools, elementary patterns and skill fade, skill categorisation has been able to be introduced into the CodeWOF system. 
  
  
