title: "Green Fibre - Sustainable Connectivity"
name: "Joshua Yee"
sponsor: "Enable Fibre Broadband"
supervisor: "Andreas Willig"
type: industry
abstract: | 
  Enable Fibre own and operate the fibre broadband network in Christchurch. With over 200,000 homes, businesses and schools connected to their network. It is a goal of Enable Fibre to ensure that they are meeting requirements from both the government and their customers in the environmental sustainability sector. 
  Enable Fibre has initiated the Green Fibre project, which aims to help understand, model and measure the environmental impact/sustainability of their fibre network. The outcome of this project is to create a research paper containing a robust methodology of measuring the environmental impact of the fibre network, with research completed on alternatives such as copper and fixed wireless networks. A small software artefact has also been produced in Microsoft VBA that models the environmental impact of a current or hypothetical fibre zone. The Green Fibre project will allow Enable to utilise the provided research and artefact to assist making future decisions surrounding sustainable best practices.

